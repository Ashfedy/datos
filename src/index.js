/* eslint-disable import/default */

import 'babel-polyfill';
import React from 'react';
import {render} from 'react-dom';
import configureStore from './store/configureStore';
import {Provider} from 'react-redux';
import { BrowserRouter, Route } from "react-router-dom";
import App from './components/App';
import {getProductList} from './actions/productAction';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './styles/css.css';
import './styles/websitedark.css';

const store = configureStore();
store.dispatch(getProductList());

render(
  <BrowserRouter>
    <Provider store={store}>
      <Route path="/" component={App}/>
    </Provider>
  </BrowserRouter>,
    document.getElementById('datos')
);
