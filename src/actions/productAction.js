import * as types from './actionTypes';

export function getProductListSuccess(products) {
  return {type: types.FETCH_PRODUCTS, products}
}

export function getProductList() {
  return function(dispatch) {
    return fetch('https://s3.ap-south-1.amazonaws.com/cryptolink/essentials/product_list.json',{
      method: "GET"
    })
    .then((response) => response.json())
    .then(response => {
      dispatch(getProductListSuccess(response));
    }).catch(error => {
      console.log("Something went wrong!");
      throw(error);
    });
  };
}
