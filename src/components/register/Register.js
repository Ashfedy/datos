import React from 'react';
import PropTypes from "prop-types";

class Register extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.mail = this.mail.bind(this);
  }

  mail() {
    let registeredName = $('#registeredName').val();
    let registeredEmail = $('#registeredEmail').val();
    let registeredCompany = $('#registeredCompany').val();
    let registeredMessage = $('#registeredMessage').val();
    var req_data = new FormData();
    req_data.append('name',registeredName);
    req_data.append('email',registeredEmail);
    req_data.append('company',registeredCompany);
    req_data.append('message',registeredMessage);
    if(registeredName != '' && registeredEmail != '' && registeredCompany != '' && registeredMessage != '')
    {
      $.ajax({
        url: '/send_mail.php',
        data: req_data,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(data){
          alert(data);
        }
      });
    }
    else {
      alert('Please fill in all fields')
    }
    $('#registeredName').val("");
    $('#registeredEmail').val("");
    $('#registeredCompany').val("");
    $('#registeredMessage').val("");
  }

  render() {
    return (
      <div className="register-wrapper">
        <div className="iecontent container content">
          <div className="row">
            <div className="col-lg-8 col-md-8 col-sm-8 col-xs-12">
           <h1>DATOS Partner Program</h1>
           <p>We want our partners to be successful and provide high-level quality to the customers. That’s why we introduced our DATOS-CM & DATOS-BM Partner Program, which contains <a href="#greatAdvantages">great advantages</a></p>
           <ul>
            <li>Free training in <a href="#programmingTool">Programming Tools</a> </li>
            <li>Free unlimited support</li>
            <li>Free Programming Tool</li>
            <li>Free software updates</li>
            <li>Free annual workshop</li>
            <li>Special partner multiplier based on annual sales</li>
            <li>Your company logo on our products (optional)</li>
           </ul>
           <p>We are continuously looking for system integrators & dealers that are willing to become our partner.</p>
           <h2>Partnerships</h2>
           <p>There are two different forms of partnership: DATOS Certified Partner and DATOS Certified Knowledge Center. Both profit from the benefits mentioned above, only the Knowledge Center also allows the use of our innovative program.</p>
           <h2>The reason of our Partner Program</h2>
           <p>Although we recognize the intimate relationship between profitability and quality, we know that our partner’s success is ultimately depending on the well-being and innovative character of its engineers. We want to develop a successful, long term, strategic relationship based on achieving best practice and sustainable competitive advantage.</p>
           <h2>Training</h2>
           <div id="greatAdvantages"><p>To be successful as a partner and provide high-level quality to the customers, training is needed. DATOS provides trainings and workshops to keep the knowledge of our partners at the highest level.</p></div>
           <h2>Open Source</h2>
           <p>DATOS recognizes that open building solutions combined with an efficient and easily understood engineering framework is crucial for in-house engineers and building owners. That is why we market an, already proven, open source technology.</p>
           <p>Once you’ve signed up as a Datos Partner, you will profit from many benefits.</p>
           <ul>
             <li><strong>Free training in CPT tools &amp; the FG series</strong> <br/>During our CPT tools &amp; the FG series training you will learn how to work with the CPT tool and program our FG series controllers.<br/><br/></li>
             <li><strong>Free unlimited support</strong> <br/>When you encounter problems our support team will help you fix them. The support is always free, no matter how many times you need it.<br/><br/></li>
             <li><strong>Free CPT tool</strong> <br/>The CPT tool is your engineering tool that does everything... and more.<br/><br/></li>
             <li><strong>Free software updates</strong> <br/>We continuously update the software that runs on our products. As a partner these updates are free.<br/><br/></li>
             <li id="programmingTool"><strong>Free annual workshop</strong> <br/>Handy to update your knowledge on CPT &amp; the FG Series and EasyStack, but also ideal to learn about new features.<br/><br/></li>
             <li><strong>Special partner multiplier based on annual sales</strong> <br/>The more Datos products you sell, the better your price will be.<br/><br/></li>
           </ul>
           <p>Programming Tools We know that our partners always want to get the best out of the products they buy. That’s why we encourage training. With this training you really get to know PT tools and the EasyIO F-series and that contributes to providing the high-level quality you want to your customers. For training dates and general information about trainings, please look at the training page.</p>
           <p>This PT Tools & DATOS-BM training consist of:</p>
           <h2>Introduction</h2>
           <p>You cannot take advantage of our products if you don’t know them. This introduction shows our main products to you and explains how our DATOS works.</p>
           <h2>PT Tool Overview</h2>
           <p>Programming Tools is an open source software programming tool that provides third party configuration and management tools for products that run in a Sedona environment. PT Tools has the ability to configure and manage the web server on the DATOS-BM.</p>
           <h2>Sedona Basics</h2>
           <p>Sedona is the open source programming language. that provides third party configuration and management tools for products that run in the Sedona environment. It is designed to make it easy to build smart.</p>
           <h2>Kit Management</h2>
           <p>Kit Management in PT Tools is used to manage the Sedona kits in a Sedona controller. The Kit Manager will display all the kits that are installed in the PT Tools Sedona folder or in the Sedona folder selected.</p>
           <h2>Build Sedona Control Programs</h2>
           <p>This topic consists of building simple AHU control applications, like for ventilation, temperature, mix damper (CO2 control) and water production (heating and cooling).</p>
           <h2>Create your own library </h2>
           <p>PT Tools allows you to create a template library for future use. This feature reduces Sedona programming time. You can also create libraries for graphics. </p>
           <h2>HTML 5 Graphics pages for web browser </h2>
           <p>Create multiple graphics pages in the DATOS for the Sedona control programs that were built earlier. </p>
           <h2>Charts </h2>
           <p>The SQL Lite is capable of storing history into tables and columns. SQL Lite table data can be displayed in PT web graphics. </p>
           <h2>Backup Management </h2>
           <p>The PT Tools web server has another option for ‘backups and restores’ of the Sedona application. This option backups the Sedona application as well as the graphics from the SD card.  </p>
           <h2>Networks </h2>
           <p>Configure network communications, like Modbus, BACnet and P2P. </p>
         </div>
         <div className="col-lg-4 col-md-4 col-sm-4 hidden-xs infopageimagebar">
           <div className="company-form">
             <h2>Register as Partner</h2>
             <div className="contact-us-form-container">
             <p>If you are interested in becoming a Datos Partner, please mail us and we will get in contact with you as soon as possible.</p>
               <div className="form-group">
                 <input id="registeredName" type="text" name="name" placeholder="Name"/>
               </div>
               <div className="form-group">
                 <input id="registeredEmail" type="email" name="email" placeholder="Email"/>
               </div>
               <div className="form-group">
                 <input id="registeredCompany" type="text" name="company" placeholder="Company"/>
               </div>
               <div className="form-group">
                 <textarea id="registeredMessage" name="message" placeholder="Message"></textarea>
               </div>
               <div className="form-group">
                 <button className="mail-btn" id="mailBtn" onClick={this.mail}>Send Your Enquiry</button>
               </div>
             </div>
           </div>
         </div>
        </div>
        </div>
      </div>
    );
  }
}

Register.propTypes = {

};


export default Register;
