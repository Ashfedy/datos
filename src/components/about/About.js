import React from 'react';
import {Link} from 'react-router';

const About = () => {
  const init_wow = () => {
    new WOW().init();
  }
    return (
      <div className="about-wrapper">
      <div>{init_wow()}</div>
      <div className="iecontent container content">
    <h1 className="text-center">About DATOS</h1><br/>
    <h2>Leading the way in technology</h2>
    <p>DATOS-IoT is an automation division of WatchNET Inc, Canada provides innovative products for Building Management System as well as Critical & Infrastructure Monitoring Solution. DATOS Building Management Systems provide a strong foundation for intelligent buildings that inspire occupant productivity and deliver optimal energy and operational efficiency.</p><p>The key elements of our product & solutions are; economical, high energy savings, fewer devices and software, multiple protocols & easy integration.</p>
    <h2>Innovation & Technology</h2>
    <p>DATOS always wanted to be unique, innovative & stand-out from the crowd. Without differentiation and innovation, there is no value for any product or solution. We see paradigm shift by M2M (machine to machine), building automation and energy management segment. We envisage a great prospect and provide much better solutions by out-of-the-box philosophy that are presently are provided.</p><p>We diverse ourselves from the typical approach and take the advantage of IP and Wireless technology from the conventional method with the intention of drive from devices to enterprise and deploy much advanced analytical tools and GUI.</p>
    <h2>Market focus</h2>
    <p>Obviously, we are in the BMS market, but it is not our total focus. Additionally, our target is mid-sized buildings where currently 70 to 80% of the buildings are not connected to an amplified system so that they can be analyzed, monitored and measured or benchmarked since the main reason is that they are not economical.</p>
    <p>We put ourselves in customer’s perception and focusing to reduce costs and we do that by having a shorter development cycle to getting products to market.</p>
    <h2>Inspiring Approach</h2>
    <p>Datos takes the path of inspiring approach to the market and to everything certainly. Our team is our strength and all the people working in Datos have great experience working in BMS and M2M and they understand every aspect of the business and technology. Our team have wide knowledge and better understanding on data, web technologies, marketing and have an open mind towards progress. We are looking at customer’s perspective and understand their needs while looking at how we can do things differently, easier, more cost effectively than the traditional systems.</p>
    <h2>Best Operational Attitude</h2>
    <p>When designing and installing a BMS there needs to be a mutual expectation of performance, after the system is commissioned, witnessed and handed over. These can then be monitored, tested and analyzed thereafter, just like we would do with any other valuable building asset.</p>
    <p>Our knowledge base gives us cutting edge strength in development and delivery of comprehensive, integrated, end-to-end solutions for projects in the area of automation. We have the capability of executing any scale of project.</p>
</div>
      </div>
    );
};
export default About;
