import React from "react";
import PropTypes from "prop-types";
import { Switch, Route } from "react-router-dom";
import Header from './common/header/Header';
import Footer from './common/footer/Footer';
import Home from './home/Home';
import About from './about/About';
import Register from './register/Register';
import Product from './products/Product';
import DatosCategory from './products/DatosCategory';
import ProductSection from './products/ProductSection';
import ProductDescription from './products/ProductDescription';
import Technology from './technology/Technology';
import Training from './training/Training';
import NotFound from './home/NotFound';

const App = ({ location}) => {
  return (
  <div className="datos-wrapper">
  <div className="datos-wrapper-bg"></div>
    <Header/>
    <div className="min-height-wrapper">
      <Switch>
        <Route location={location} path="/" exact component={Home} />
        <Route location={location} path="/about" exact component={About} />
        <Route location={location} path="/register" exact component={Register} />
        <Route location={location} path="/technology" exact component={Technology} />
        <Route location={location} path="/training" exact component={Training} />
        <Route location={location} path="/product" exact component={Product} />
        <Route location={location} path="/product/:category" exact component={DatosCategory} />
        <Route location={location} path="/product/:category/:section" exact component={ProductSection} />
        <Route location={location} path="/product/:category/:section/:model" exact component={ProductDescription} />
        <Route component={NotFound}/>
      </Switch>
    </div>
    <Footer/>
  </div>
);
};

App.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired
};

export default App;
