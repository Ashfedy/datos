import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router';

const ProductSectionItem = ({product_details}) => {
  return (
    <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <a href={"/"+product_details.section_url+"/"+product_details.section_name}>
        <div className="product-section-item">
          <p className="datos-section-item-title">{product_details.title}</p>
          <p className="datos-section-item-count">Products : {product_details.products.length}</p>
        </div>
      </a>
    </div>
  );
};
ProductSectionItem.propTypes = {
  product_details: PropTypes.object
};

export default ProductSectionItem;
