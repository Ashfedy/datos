import React from 'react';
import PropTypes from "prop-types";
import ProductSectionItem from './ProductSectionItem';
import Slider from 'react-slick';

const ProductList = ({product_list}) => {

  return (
    <div className="product-container container">
      <h1 className="product-section-main-title">Building Automation</h1>
      <h1 className="text-center product-section-title">DATOS - BM</h1>
      <div className="product-section datos-bm-section row">
        {product_list.datos_bm.map(product =>
          <ProductSectionItem key={product.section_name} product_details={product}/>
        )}
      </div>
      <div className="product-section datos-cm-section row">
        <h1 className="product-section-main-title">Environmental Monitoring</h1>
        <h1 className="text-center product-section-title">DATOS - CM</h1>
          {product_list.datos_cm.map(product =>
            <ProductSectionItem key={product.section_name} product_details={product}/>
          )}
      </div>
      <div className="product-section datos-xs-section row">
        <h1 className="product-section-main-title">Software Platform</h1>
        <h1 className="text-center product-section-title">DATOS - ATS</h1>
          {product_list.datos_ats.map(product =>
            <ProductSectionItem key={product.section_name} product_details={product}/>
          )}
      </div>
    </div>
  );
};

ProductList.propTypes = {
  product_list: PropTypes.object
};

export default ProductList;
