import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router';

const ProductItem = ({product_details}) => {

  return (
      <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div className="datos-product">
          <a href={"/"+product_details.prod_url+"/"+product_details.model}>
            <p className="datos-product-title">{product_details.title}</p>
            <img src={product_details.image_url+"images/"+product_details.image}/>
            <p className="datos-product-model">{product_details.model}</p>
          </a>
        </div>
      </div>
  );
};
ProductItem.propTypes = {
  product_details: PropTypes.object
};

export default ProductItem;
