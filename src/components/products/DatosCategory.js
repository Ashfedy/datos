import React from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import ProductSectionItem from './ProductSectionItem';

class DatosCategory extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      category : props.match.params.category,
      section : props.match.params.section
    }
    this.title = this.title.bind(this);
    this.mainTitle = this.mainTitle.bind(this);
    this.category_description = this.category_description.bind(this);
  }

  title() {
    if(this.props.match.params.category.toLowerCase() == 'datos_bm') {
      return "DATOS - BM";
    }
    else if(this.props.match.params.category.toLowerCase() == 'datos_cm'){
      return "DATOS - CM";
    }
    else if(this.props.match.params.category.toLowerCase() == 'datos_ats'){
      return "DATOS - ATS";
    }
    else {
      return "";
    }
  }

  mainTitle() {
    if(this.props.match.params.category.toLowerCase() == 'datos_bm') {
      return "Building Automation";
    }
    else if(this.props.match.params.category.toLowerCase() == 'datos_cm'){
      return "Environmental Monitoring";
    }
    else if(this.props.match.params.category.toLowerCase() == 'datos_ats'){
      return "Software Platform";
    }
    else {
      return "";
    }
  }

  category_description() {
    if(this.props.match.params.category.toLowerCase() == 'datos_bm') {
      return "DATOS-BM offers Building Automation Solution combine an open building management platform with energy-saving HVAC, Lighting Control, Pneumatic Control, Chillers and Fire System Integration to enable smarter & greener buildings. Datos Building Management (DATOS-BM) controls the complete automation systems on a single IP-based management platform based on standard international protocols and offers easy maintenance with device configuration auto-backup and restore function. DATOS-BM gathers global energy data and provides analytical measurement and verification tools. With substantial system integration expertise DATOS-BM assures building safety with Intelligent Video Surveillance & Access Control System Technologies.";
    }
    else if(this.props.match.params.category.toLowerCase() == 'datos_cm'){
      return "DATOS-CM offers wide range of innovative solutions for Critical Infrastructure Monitoring, Data Center & Environmental Monitoring Systems and Early Detection Products & Solutions. DATOS IoT manufactures complete range of monitoring systems that enable customers to monitor their facilities through a web browser and sensor data for factors like temperature, humidity, airflow, light, sound, power, water leakage and more, showing current and historical data and providing Email, SMS and SNMP alert notifications under abnormal conditions.";
    }
    else if(this.props.match.params.category.toLowerCase() == 'datos_ats'){
      return "The DATOS-ATS-ES is a powerful embedded Data Acquisition System ready for industrial deployment. It comes pre-installed with the full DATOS Automation Software platform and is ready to perform a number of tasks. Designed for OEM’s and System Integrators, the DATOS-ATS-ES is fully customizable, very cost effective, and includes all the DATOS features and modules at no extra cost. Being a Linux server, users are given root access and can extend the DATOS-ATS-ES to host their services as well or take advantage of the numerous Linux tools included. The DATOS-ATS-ES is the perfect choice for small to medium standalone or edge applications with less than 3000 data points and less than 10 users. There is a din rail clip on the back of each DATOS-ATS-ES for easy mounting directly in a control panel.";
    }
    else {
      return "";
    }
  }

  render() {
    return (
       <div className="product-container container">
       {this.props.products[this.state.category].length != 0 && <ol className="breadcrumb">
         <li><a href="/product/">Products</a></li>
         <li className="">{this.title()}</li>
       </ol>}
       {this.props.products[this.props.match.params.category.toLowerCase()] != undefined ? <h1 className="text-center product-section-main-title">{this.mainTitle()}</h1>: <h1 className="text-center">NOT FOUND</h1>}
        {this.props.products[this.props.match.params.category.toLowerCase()] != undefined ? <h1 className="text-center product-section-title">{this.title()}</h1>: <h1 className="text-center">NOT FOUND</h1>}
        {this.props.products[this.props.match.params.category.toLowerCase()] != undefined ? <p className="product-section-description">{this.category_description()}</p>:""}
        {this.props.products[this.props.match.params.category.toLowerCase()] != undefined && this.props.match.params.category.toLowerCase() == 'datos_bm' && <div className="product-section-sub-description">
          <p>Thus, it controls all compatible protocols available on the market:</p>
          <ol>
            <li>BACnet-MSTP / BACnet-IP</li>
            <li>M Bus</li>
            <li>OPC</li>
            <li>Modbus (485 / IP)</li>
            <li>DALI</li>
            <li>Haystack</li>
            <li>SNMP</li>
            <li>SQL</li>
            <li>KNX</li>
          </ol>
        </div>}
        {this.props.products[this.props.match.params.category.toLowerCase()] != undefined && this.props.match.params.category.toLowerCase() == 'datos_cm' && <div className="product-section-sub-description">
          <p>Our range of solutions includes…</p>
          <ol>
            <li>Environmental Protection for Large Data Centers</li>
            <li>Power Environment Monitoring</li>
            <li>Precision Water Leak Detection System</li>
            <li>Central Battery Monitoring System</li>
            <li>Intelligent Air Conditioning Controller</li>
            <li>Particle & Gas Detection System</li>
            <li>Lighting Control System</li>
          </ol>
        </div>}
        <div className="product-section datos-bm-section row">
          {this.props.products[this.props.match.params.category.toLowerCase()] != undefined && this.props.products[this.props.match.params.category.toLowerCase()].map(product =>
            <ProductSectionItem key={product.section_name} product_details={product}/>
          )}
        </div>
       </div>
    );
  }
}

DatosCategory.propTypes = {
  products: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  return {
    products: state.products
  };
}

export default connect(mapStateToProps)(DatosCategory);
