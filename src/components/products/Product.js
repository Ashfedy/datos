import React from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import ProductList from './ProductList';

class Product extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div className="product-wrapper">
        <ProductList product_list={this.props.products}/>
      </div>
    );
  }
}

Product.propTypes = {
  products: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  return {
    products: state.products
  };
}

export default connect(mapStateToProps)(Product);
