import React from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import ProductDescriptionView from './ProductDescriptionView';

class ProductDescription extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      category : props.match.params.category,
      section : props.match.params.section,
      model: props.match.params.model
    }
    this.mail = this.mail.bind(this);
    this.title = this.title.bind(this);
  }

  title() {
    if(this.props.match.params.category.toLowerCase() == 'datos_bm') {
      return "DATOS - BM";
    }
    else if(this.props.match.params.category.toLowerCase() == 'datos_cm'){
      return "DATOS - CM";
    }
    else if(this.props.match.params.category.toLowerCase() == 'datos_ats'){
      return "DATOS - ATS";
    }
    else {
      return "";
    }
  }

  mail() {
    let Name = $('#Name').val();
    let Email = $('#Email').val();
    let Company = $('#Company').val();
    let Message = $('#Message').val();
    let Type = $('#Type').val();
    var req_data = new FormData();
    req_data.append('name',Name);
    req_data.append('email',Email);
    req_data.append('company',Company);
    req_data.append('message',Message);
    req_data.append('type',Type);
    if(Name != '' && Email != '' && Company != '' && Message != '')
    {
      $.ajax({
        url: '/send_quote.php',
        data: req_data,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(data){
          alert(data);
        }
      });
    }
    else {
      alert('Please fill in all fields')
    }
    $('#Name').val("");
    $('#Email').val("");
    $('#Company').val("");
    $('#Message').val("");
    $('#Type').val("");
  }

  render() {
    return (
      this.props.products[this.state.category].length != 0 && <ProductDescriptionView mail={this.mail} category={this.state.category} section={this.state.section} category_name={this.title()} section_name={this.props.products[this.state.category].filter(section => section.section_name == this.state.section)[0].title} product_details={this.props.products[this.state.category].filter(section => section.section_name == this.state.section)[0].products.filter(product => product.model == this.state.model )[0]}/>
    );
  }
}

ProductDescription.propTypes = {
  products: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  return {
    products: state.products
  };
}

export default connect(mapStateToProps)(ProductDescription);
