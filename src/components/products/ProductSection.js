import React from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import ProductItem from './ProductItem';

class ProductSection extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      category : props.match.params.category,
      section: props.match.params.section
    }
    this.title = this.title.bind(this);
  }

  title() {
    if(this.props.match.params.category.toLowerCase() == 'datos_bm') {
      return "DATOS - BM";
    }
    else if(this.props.match.params.category.toLowerCase() == 'datos_cm'){
      return "DATOS - CM";
    }
    else if(this.props.match.params.category.toLowerCase() == 'datos_ats'){
      return "DATOS - ATS";
    }
    else {
      return "";
    }
  }

  render() {
    return (
      <div className="product-container container">
        {this.props.products[this.state.category].length != 0 && <ol className="breadcrumb">
          <li><a href="/product/">Products</a></li>
          <li><a href={"/product/"+this.state.category}>{this.title()}</a></li>
          <li className="">{this.props.products[this.state.category].filter(section => section.section_name == this.state.section)[0].title}</li>
        </ol>}
       {this.props.products[this.state.category].length != 0 && <h1 className="text-center product-section-title">{this.props.products[this.state.category].filter(section => section.section_name == this.state.section)[0].title}</h1>}
       {this.props.products[this.state.category].length != 0 && this.props.products[this.state.category].filter(section => section.section_name == this.state.section)[0].products.map(product =>
         <ProductItem key={product.model} product_details={product}/>
       )}
      </div>
    );
  }
}

ProductSection.propTypes = {
  products: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  return {
    products: state.products
  };
}

export default connect(mapStateToProps)(ProductSection);
