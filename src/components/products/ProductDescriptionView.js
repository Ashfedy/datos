import React from 'react';
import PropTypes from "prop-types";
// import {Link} from 'react-router';
// import ProductFeatures from './ProductDescriptionComponents/ProductFeatures';

const ProductDescriptionView = ({mail, product_details, category, section, category_name, section_name}) => {
  return (
    <div className="product-details-container container">
      <ol className="breadcrumb">
        <li><a href="/product/">Products</a></li>
        <li><a href={"/product/"+category}>{category_name}</a></li>
        <li><a href={"/product/"+category+"/"+section}>{section_name}</a></li>
        <li className="">{product_details.title}</li>
      </ol>
      <div className="product-image-container wow flipInX"><img className="" src={product_details.image_url+"images/"+product_details.image}/></div>
      <div className="product-title text-center"><h1>{product_details.title}</h1></div>
      <div className="product-model text-center"><h2>{product_details.model}</h2></div>
      <hr/>
      <div className="product-description">
        <h2>Description</h2>
        {product_details.description.map( (desc, index) =>
          <p key={index}>{desc}</p>
        )}
      </div>
      <hr/>
      {product_details.features.length != 0 && <div className="product-features">
        <h2>Features</h2>
        <ul>
          {product_details.features.length != 0 && product_details.features.map( (feature, index) =>
            <li key={index}>{feature}</li>
          )}
        </ul>
        <hr/>
      </div>}
      {product_details.characteristics.length != 0 && <div className="product-characteristics">
        <h2>Characteristics</h2>
        <ul>
          {product_details.characteristics.length != 0 && product_details.characteristics.map( (characteristic, index) =>
            <li key={index}>{characteristic}</li>
          )}
        </ul>
        <hr/>
      </div>}
      {product_details.applications.length != 0 && <div className="product-application">
        <h2>Applications</h2>
        {product_details.applications.length != 0 && product_details.applications.map( (application, index) =>
          <p key={index}>{application}</p>
        )}
      </div>}
      <div className="product-download">
        <h2>Download</h2>
          <a href={"/"+product_details.model+".pdf"} target="_blank">{"Download Data Sheets - "+product_details.model}</a>
      </div>
      <div className="request-quote-section">
        <h2>Request quote</h2>
        <p>To request a quote for this product, simply fill in your name, company and e-mail address below and we will send you a quote as soon as possible.</p>
        <div className="form-group">
          <input id="Name" type="text" name="name" placeholder="Name"/>
        </div>
        <div className="form-group">
          <input id="Email" type="email" name="email" placeholder="Email"/>
        </div>
        <div className="form-group">
          <input id="Company" type="text" name="company" placeholder="Company"/>
        </div>
        <div className="form-group">
          <input id="Type" name="type" type="hidden" value={product_details.model}/>
        </div>
        <div className="form-group">
          <input id="Message" name="message" type="text" placeholder="Message"/>
        </div>
        <div className="form-group">
          <button className="mail-btn btn" id="mailBtn" onClick={mail}>Send Your Enquiry</button>
        </div>
      </div>
    </div>
  );
};
ProductDescriptionView.propTypes = {
  product_details: PropTypes.object,
  mail: PropTypes.func,
  category: PropTypes.string,
  section: PropTypes.string,
  section_name: PropTypes.string,
  category_name: PropTypes.string
};

export default ProductDescriptionView;
