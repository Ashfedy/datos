import React from 'react';

const Contact = () => {
  return (
    <div className="contact-us-wrapper">
      <p>Please contact me here.</p>
    </div>
  );
};

export default Contact;
