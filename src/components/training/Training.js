import React from 'react';
import PropTypes from "prop-types";

class Training extends React.Component {
  constructor(props, context) {
    super(props, context);
    new WOW().init();
  }

  render() {
    return (
       <div className="training-wrapper">
       <div className="iecontent container content training-container">
 <div className="row">
   <div className="col-lg-8 col-md-8 col-sm-8 col-xs-12">
     <h1>Training</h1>
     <p>The world is becoming smarter and greener by the implementation of right automation systems. Building Management Systems & Critical Infrastructure Monitoring System leads to more optimized control of resources, high end security and ease of access from anywhere. The internet of things initiated by big technology ventures will create a smart world which can be controlled by just a click. The lack of trained BMS & Automation technicians leads to the improper design and implementation.</p>
     <h2>Training Course Verticals:</h2>
     <p>DATOS conducts training in both Building Management System and Critical Infrastructure Monitoring Solutions. This course would suit system integrators in the BMS platform and the training program makes you fundamental understanding of control processes and application of Direct Digital Control (DDC) in the HVAC sector, or people seeking to enter the field of Building Management System (BMS) controls, Critical Infrastructure Monitoring Systems, Servicing and Engineering.</p>
     <p>DATOS training course inclusions are as follows</p>
     <h1>DATOS-BM</h1>
     <strong>Benefits of the BMS Controls Training Includes:</strong>
     <ol>
      <li>Learning HVAC Principles and Product Ranges that best suit each application</li>
      <li>Gaining comprehensive knowledge on specific products</li>
      <li>Expanding your Hardware / Software product skill set</li>
      <li>Open new Building Management System (BMS) project possibilities</li>
      <li>Highlight key aspects of product installation</li>
      <li>Learning Integration Fundamentals and techniques to gain you an advantage in the market</li>
      <li>DATOS-ATS software and data-acquisition training</li>
     </ol>
     <h1>DATOS-CM</h1>
     <strong>Benefits of the CIMS (Critical Infrastructure Monitoring System) Training Includes:</strong>
     <ol>
      <li>Environmental Protection for Large Data Centers</li>
      <li>Power Environment Monitoring</li>
      <li>Precision Water Leak Detection System</li>
      <li>Central Battery Monitoring System</li>
      <li>Intelligent Air Conditioning Controller</li>
      <li>Particle & Gas Detection System</li>
      <li id="training&registration">Lighting Control System</li>
     </ol>
     <p><strong>By submitting this training registration form you confirm you have read and agree to the DATOS <a href="#training&registration">Training Registration Terms & Conditions</a></strong></p>
     <strong>Training Registration Terms & Conditions</strong>
     <p><u>After submitting a training registration form on the DATOs website, the submitter (from now on referred to as ‘partner) will:</u></p>
     <ul>
     <li>Receive an email from DATOS. </li>
     <li>Be asked to submit travel details (date, time and place of arrival and depart), when training dates are confirmed</li>
     <li>Receive a final confirmation email, when all personal and travel details are sent to DATOS. </li>
     </ul>

     <p><u>By filling in the training registration form on the DATOS website the partner agrees to the following conditions:</u></p>
     <ul>
       <li>To be able to register for and attend a training, partner’s company should have an DATOS Certified Partner agreement.</li>
       <li>The training is free of charge for DATOS Certified Partners.</li>
        <li>A training registration is final, after the partner has received a final confirmation email from DATOS.</li>
        <li>Cancellation of a (confirmed) training registration must be done in writing by sending an email to training@datosiot.com.</li>
       <li>Cancellation of a (confirmed) training registration must be done at least two weeks (14 days) prior to the start of this training.</li>
       <li>If cancellation of an DATOS training is done later than two weeks (14 days) prior to the start of the training, costs of the course itself will still be invoiced to the partner.</li>
       <li>DATOS reserves the right to cancel a training at all times and is not liable for any travel, booking and/or other costs made by any attendee of the training.</li>
     </ul>
   </div>
   <div className="col-lg-4 col-md-4 col-sm-4 hidden-xs infopageimagebar">
     <a href="/" className="fancybox" rel="images1"><img src="../../images/training1.gif" className="wow fadeIn" alt=""/></a>
     <a href="/" className="fancybox" rel="images1"><img src="../../images/training2.png" className="wow fadeIn" alt=""/></a>
     <a href="/" className="fancybox" rel="images1"><img src="../../images/training3.jpg" className="wow fadeIn" alt=""/></a>
   </div>
 </div>
</div>
       </div>
    );
  }
}

Training.propTypes = {
};

export default Training;
