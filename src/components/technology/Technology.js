import React from 'react';
import PropTypes from "prop-types";

class Technology extends React.Component {
  constructor(props, context) {
    super(props, context);
    new WOW().init();
  }

  render() {
    return (
       <div className="technology-wrapper">
       <div className="iecontent container content">
 <div className="row">
   <div className="col-lg-8 col-md-8 col-sm-8 col-xs-12">
     <h1>Technologies</h1>
     <p>Time ahead and within few years; the M2M technology will become Smaller, Deeper and Wider. Large enterprises will look at smart grid opportunities and create things very smarter, intelligent and more stand-alone by taking advantage of wireless and IP technology. The world is moving towards Web 2.0 with better internet speed and availability as this development could then lead to better communication between systems.</p>

     <p><strong>Machine to Machine Technology</strong><br/>Time ahead and within few years; the M2M technology will become Smaller, Deeper and Wider. Large enterprises will look at smart grid opportunities and create things very smarter, intelligent and more stand-alone by taking advantage of wireless and IP technology. The world is moving towards Web 2.0 with better internet speed and availability as this development could then lead to better communication between systems.<br/><br/>We always wanted to align with IP technology as the customers & users will definitely benefit from that because they will get value for their investment, more control over their facilities by using advanced technology where they didn’t benefit earlier.</p><br/>
     <p><strong>BMS versus M2M</strong><br/>BMS system is about a technology centered on building automation but with M2M helps how can we get the devices more smarter, intelligent, cost effective and smaller so that they can communicate each other well and perform on their own efficiently. In a nutshell, the equipment can take their own decisions faster and efficient without much supervisory unlike BMS systems that need more supervisory & controls since BMS need more powerful system and doesn’t have free framework within. BMS is all about the building.</p><br/>
     <p><strong>Datos Freeboard</strong><br/>The key is: it’s all about simplicity! There are a lot of tools on the market that are very good but you have to be a software programmer to use them. They have their place in the market. At the large enterprise level they have a staff of people who can do that.<br/><br/>But most people who want to look at energy, at performance, are mainly non-technical people. They might know how to use a smartphone, Ipad or android and a dashboard like Datos Freeboard, which is very user friendly, can have these people look at their energy usage and monitor and influence them. People can see the performance of one building or many, without spending a lot of money, set environmental and energy targets and are able to benchmark this performance.<br/><br/>The nice thing is that Datos will go direct to your browser or any server, directly through the Internet without any additional devices or difficult protocols.</p><br/>
   </div>
   <div className="col-lg-4 col-md-4 col-sm-4 hidden-xs infopageimagebar">
    <a href="/" className="fancybox" rel="images1"><img src="../../images/tech1.png" alt="" className="zoomIn wow"/></a><a href="/" className="fancybox" rel="images1"><img src="../../images/tech3.jpg" className="zoomIn wow" alt=""/></a>
   </div>
 </div>
</div>
       </div>
    );
  }
}

Technology.propTypes = {

};


export default Technology;
