import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router-dom';
import '../../styles/datos.css';
import Slider from 'react-slick';

const Home = () => {
  const init_wow = () => {
    new WOW().init();
  }

  const settings = {
      dots: false,
      infinite: true,
      speed: 1000,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      autoplay: true,
      pauseOnHover: false
    };
  return(
    <div className="homepage-wrapper">
    <div>{init_wow()}</div>
      <div className="first-fold">
        <div className="carousel-container">
          <Slider {...settings}>
          <img src="../../images/homepage_bg1.jpg" className=""/>
          <img src="../../images/homepage_bg2.jpg" className=""/>
          <img src="../../images/homepage_bg3.jpg" className=""/>
          <img src="../../images/homepage_bg4.jpg" className=""/>
          </Slider>
        </div>
      </div>
      <div className="third-fold">
        <div className="third-fold-content">
          <h1>ABOUT US</h1>
          <p>DATOS is an automation division of WatchNET Inc, Canada provides innovative products for Building Management System as well as Critical & Infrastructure Monitoring Solution. DATOS Building Management Systems provide a strong foundation for intelligent buildings that inspire occupant productivity and deliver optimal energy and operational efficiency.</p>
          <p>The key elements of our product & solutions are; economical, high energy savings, fewer devices and software, multiple protocols & easy integration.</p>
          <a href="/about">READ MORE</a>
        </div>
      </div>
      <div className="fouth-fold">
      <div className="news-section container">
      <h1 className="text-center">NEWS UPDATES</h1>
        <div className="row">
          <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div className="datos-news wow zoomInUp">
              <img src="../../images/4th-fld-img1.jpg"/>
              <p>DATOS products have been launched at Intersec’2018 show held at Dubai World Trade Center. WatchNET had launched both DATOS-BM & DATOS-CM during the show.</p>
            </div>
          </div>
          <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div className="datos-news wow zoomInUp">
              <img src="../../images/4th-fld-img2.jpg"/>
              <p>We are pleased to inform you that the processors used in our DATOS controllers are not susceptible to either the 'Meltdown' or the 'Spectre' vulnerabilities. ARM Holdings, the designer of ARM processor chips, has identified that. Their processors are built in our CM, CH, CR, and CW range of products.</p>
            </div>
          </div>
          <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div className="datos-news wow zoomInUp">
              <img src="../../images/4th-fld-img3.jpg"/>
              <p>DATOS conducts professional training in both Building Management System and Critical Infrastructure Monitoring Solutions. This course would suit system integrators in the BMS platform and the training program makes you fundamental understanding of control processes and application.</p>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  );
};
Home.propTypes = {
  // name: PropTypes.string.isRequired
};

export default Home;
