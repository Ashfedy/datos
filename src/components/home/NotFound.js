import React from 'react';

const NotFound = () => {
    return (
      <div className="not-found-wrapper">
        <div className="container not-found-wrapper-container">
          <div className="not-found-wrapper-section">
            <h1 className="text-center">Not Found</h1>
            <div className="not-found-image-container">
              <img src="https://s3.ap-south-1.amazonaws.com/cryptolink/essentials/404.gif"/>
            </div>
          </div>
        </div>
      </div>
    );
};

export default NotFound;
