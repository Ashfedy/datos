import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router-dom';

const HeaderRightLink = () => {
  return (
    <div className="navbar-collapse collapse menu-bar">
        <ul className="nav navbar-nav">

            <li className="active home"><a href="/">Home</a></li>
            <li className="about"><a href="/about">About us</a></li>
            <li className="register"><a href="/register">Become partner</a></li>
            <li className=""><Link to="/product">Products<span className="glyphicon glyphicon-chevron-down"></span></Link>
                <ul className="css-header">
                    <li className=""><a href="/product/datos_bm">Building Automation</a></li>
                    <li className=""><a href="/product/datos_cm">Environmental Monitoring</a></li>
                    <li className=""><a href="/product/datos_ats">Software Platform</a></li>
                </ul>
            </li>
            <li className="technology"><a href="/technology">Technologies</a></li>
            <li className="training"><a href="/training">Training</a></li>
            <li className="contact"><a href="#contactInfo">Contact</a></li>
        </ul>
    </div>
  );
};
HeaderRightLink.propTypes = {
};

export default HeaderRightLink;
