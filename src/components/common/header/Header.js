import React from 'react';
import PropTypes from "prop-types";
// import {Link} from 'react-router';
import HeaderRightLink from './HeaderRightLink';

const Header = () => {
  return (
    <div className="datos-header-wrapper">
      <div className="navbar navbar-inverse navbar-fixed-top css-header">
        <div className="containe-fluidr">
          <div className="navbar-header">
            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            <div className="navbar-logo">
              <a href="/"><img src="/images/logo.png" alt="EasyIO"/></a>
            </div>
          </div>
          <HeaderRightLink/>
        </div>
      </div>
    </div>
  );
};
Header.propTypes = {
  // name: PropTypes.string.isRequired
};

export default Header;
