import React from 'react';
import PropTypes from "prop-types";
import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({ text }) => (
  <div style={{
    position: 'relative', color: '#fff', background: 'rgb(147, 183, 19)',
    height: 20, width: 60, top: -20, left: -30, textAlign: 'center', paddingTop: '4px'
  }}>
    {text}
  </div>
);

class FooterMiddle extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      center: {lat: 43.8181798, lng: -79.3311363},
      zoom: 11
    }
  }

  render() {
    return (
      <div className="google-maps-container">
       <GoogleMapReact
        bootstrapURLKeys={{ key: ["AIzaSyCo-2FZY0l24ScKgQUr2h18KJgwxqe3W_I"] }}
        defaultCenter={this.state.center}
        defaultZoom={this.state.zoom}
      >
        <AnyReactComponent
          lat={43.8181798}
          lng={-79.3311363}
          text={'DATOS'}
        />
      </GoogleMapReact>
      </div>
    );
  }
}

export default FooterMiddle
