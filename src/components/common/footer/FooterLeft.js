import React from 'react';
import PropTypes from "prop-types";

const FooterLeft = () => {
  return (
    <div className="footer-left">
    <div className="address-section">
      <h4>Canada:</h4>
      351 Ferrier Street,  Unit 5 Markham,<br/>
      Ontario L3R 5Z2, Canada.<br/>
      Tel: 416-410-6865<br/><br/>
    </div>
    <div className="address-section">
      <h4>USA:</h4>
      171 Cooper Avenue, Site 110 Tonawanda,<br/>
      NY 14150, USA.<br/>
      Local: 1-716-877-7277<br/><br/>
    </div>
    <div className="address-section">
      <h4>Middle East:</h4>
      P O Box No 126312, Office Suite,<br/>
      106, Oxford Towers Business Bay,<br/>
      Dubai, UAE.<br/>
      Tel: +971 4 565 7672<br/><br/>
    </div>
    <div className="address-section">
      Email: <a href="mailto:info@datosiot.com">info@datosiot.com</a>
    </div>
    </div>
  );
};
FooterLeft.propTypes = {
  // name: PropTypes.string.isRequired
};

export default FooterLeft;
