import React from 'react';
import PropTypes from "prop-types";
// import {Link} from 'react-router';

const FooterRight = ({mail}) => {
  return (
    <div className="footer-right">
      <div className="contact-us-form-container">
      <h4>Enquire Us</h4>
        <div className="form-group">
          <input id="partnerName" type="text" name="name" placeholder="Name"/>
        </div>
        <div className="form-group">
          <input id="partnerEmail" type="email" name="email" placeholder="Email"/>
        </div>
        <div className="form-group">
          <input id="partnerCompany" type="text" name="company" placeholder="Company"/>
        </div>
        <div className="form-group">
          <textarea id="partnerMessage" name="message" placeholder="Message"></textarea>
        </div>
        <div className="form-group">
          <button className="mail-btn" id="mailBtn" onClick={mail}>Send Your Enquiry</button>
        </div>
      </div>
    </div>
  );
};
FooterRight.propTypes = {
  mail: PropTypes.func.isRequired
};

export default FooterRight;
