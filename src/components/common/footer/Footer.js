import React from 'react';
import PropTypes from "prop-types";
import FooterLeft from './FooterLeft';
import FooterMiddle from './FooterMiddle';
import FooterRight from './FooterRight';

class Footer extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.mail = this.mail.bind(this);
  }

  mail() {
    let partnerName = $('#partnerName').val();
    let partnerEmail = $('#partnerEmail').val();
    let partnerCompany = $('#partnerCompany').val();
    let partnerMessage = $('#partnerMessage').val();
    //window.open('mailto:info@datosiot.com?subject=Enquiry%20for%20partnering&body=Hi,%0D%0A%0D%0AI%20have%20provided%20my%20details%20below%20for%20partnering%20with%20you.%20Please%20get%20back%20to%20me%20regarding%20the%20same.%0D%0A%0D%0AName:%20'+partnerName+'%0D%0A%0D%0AEmail:%20'+partnerEmail+'%0D%0A%0D%0ACompany:%20'+partnerCompany+'%0D%0A%0D%0AType:%20'+partnerType+'%0D%0A%0D%0AMessage:%20'+partnerMessage+'%0D%0A%0D%0A%0D%0A%0D%0AThank%20you,'+'%0D%0A%0D%0ARegards%0D%0A'+partnerName);
    var req_data = new FormData();
    req_data.append('name',partnerName);
    req_data.append('email',partnerEmail);
    req_data.append('company',partnerCompany);
    req_data.append('message',partnerMessage);
    if(partnerName != '' && partnerEmail != '' && partnerCompany != '' && partnerMessage != '')
    {
      $.ajax({
        url: '/send_mail.php',
        data: req_data,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(data){
          alert(data);
        }
      });
    }
    else {
      alert('Please fill in all fields')
    }
    $('#partnerName').val("");
    $('#partnerEmail').val("");
    $('#partnerCompany').val("");
    $('#partnerMessage').val("");
  }



  render() {
    return (
      <div className="footer-wrapper" id="contactInfo">
        <div className="footer-container">
          <h1>CONTACT US</h1>
          <div className="footer-section">
            <div className="row">
              <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <FooterLeft/>
              </div>
              <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <FooterRight mail={this.mail}/>
              </div>
              <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <FooterMiddle/>
              </div>
            </div>
          </div>
          <div className="footer-bottom">
            <p>&copy; DATOS IoT</p>
            <ul className="footer-social-icons-container">
              <li><a href="#"><img src="/images/fb.png"/></a></li>
              <li><a href="#"><img src="/images/tr.png"/></a></li>
              <li><a href="#"><img src="/images/yt.png"/></a></li>
              <li><a href="#"><img src="/images/li.png"/></a></li>
              <li><img src="/images/search.png"/></li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

Footer.propTypes = {

};

export default Footer;
